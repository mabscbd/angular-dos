import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { FormsModules, ReactiveFormsModule, FormsModule } from '@angular/forms'
import { StoreFeatureModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { Effect, EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { DestinoApiclient } from '/models/destinos-api-client.model';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import {
   DestinosViajesState,
   reducersDestinosViajes,
   intializeDestinosViajesState
   DestinoViajeEffects
   } from './models/destinos-viajes-state.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';

export const childrenRoutesVuelos: Routes =[
  { path: '', redirectTo: 'main', pathMatch: 'full'}
  { path: 'main', component: VuelosDetalleComponent },
  { path:  'mas-info', component: VuelosMasInfoComponent },
  { path: ':id', component: VuelosDetalleComponent },
  ];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaDestinosComponent},
  { path: 'destino/:id', component: DestinoDetalleComponent}
];
const routes: Routes = [
   { path:'',redirectTo: 'home', pathMatch: 'full' },
   { path: 'home', component: DestinoDetalleComponent },
   { path: 'destino/:id', component: DestinoDetalleComponent },
   { path: 'login', component: LoginComponent },           
   {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
    }
    {
      path: 'vuelos',
      component: VuelosComponent,
      canActivate: [ UsuarioLongueadoGuard ],
      children: childrenRoutesVuelos
     }
];
  // redux init
  export interface AppState {
    destinos: DestinosViajesState;
  }

const reducers: ActionReducerMap<AppState> = {
  destinos: reducersDestinosViajes
};

const reducersInitialState = {
  destinos: initializeDestinosViajesState()
};

  // redux fin init 

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
    gRxStoreModule.forRoot(reducers, (initialState: reducersInitialState)),
    EffectsModule.forRoot([DestinoViajeEffects])
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    DestinosApiclient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
