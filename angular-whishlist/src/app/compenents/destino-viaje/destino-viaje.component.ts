import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { AppState } from '../app.module';
import { VoteUpAction } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() Destino: DestinoViaje;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass ='col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {
    this.onClicked = new EventEmitter();
  }

  ngOnInit() {
  }
  
  ir() {
    this.onClicked.emit(this.Destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino))
    return false;
  }

  voteDowm() {
    this.store.dispatch(new VoteDownAction(this.destino))
    return false;
  }
}

