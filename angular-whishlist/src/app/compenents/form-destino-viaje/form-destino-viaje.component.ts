import { Component, OnInit, Output, EventEmitter, ɵConsole } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl , ValidatorFn } from '@angular/forms';
import { fromEvent, from } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse} from 'rxjs/ajax';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ajaxPut } from 'rxjs/internal/observable/dom/AjaxObservable';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit{
  @Output() onItemAdded: EventEmitter<DestinosViaje>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['',Validators.compose([
        Validators.required,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario:',form);
    });
  }
  
  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(200)
      distinctUntilChanged(),
      switchMap() => ajax('/assets/datos.json'))
      ).subscribe(ajaxResponse => {
        console.log(ajaxResponse);
        console.log(ajaxResponse.response);
        this.searchResults = ajaxResponse.response;
      });
  }

  guardar(nombre: string, url: string): boolean {
    let d = new DestinoViaje(nombre, url);
     this.onItemAdded.emit(d);
     return false;
  }

  nombreValidador(control: FormControl): {[s: string]: boolean}
    const l = Control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return {invalidNombre: true };
    }
    return null;
    }
    
    nombreValidatorParametrizable(minLong: number): validatorfn {
      return (control: FormControl):{ [s: string]: boolean } | null => {
        const l = Control.value.toString().trim().length;
        if (l > 0 && l < minLong) {
          return {minLongNombre: true };
      }
        return null;
        }
      }
  

  