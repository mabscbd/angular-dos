import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from '../models/destino-api-client.model';
import { DestinoViaje } from './../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css']
})
export class DestinoDetalleComponent implements OnInit {
  destino:DestinoViaje;

  constructor(private route: ActivatedRoute, private destinoApiclient:DestinosApiClient) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('14');
    this.destino = null;
    // this.destinosApiclient.getById(id);
  }

}
