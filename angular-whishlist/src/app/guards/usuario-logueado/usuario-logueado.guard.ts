
       import { Injectable } from '@angular/core';
       import { CanActivate, ActivateRouteSnapshot, RouterStateSnaphot } from '@angular/router';
       import { Observable } from 'rxjs';
       import { AuthService } from 'src/app/services/auth.service';

       @Injectable({
          providedIn: 'root'
       })
       export class UsuarioLogueadoGuard implements CanActivate {
         constructor(private authService: AuthService) {}
       
       canActivate(next: ActivateRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
           cons isLoggedIn = this.authService. isLoggedIn();
           console.log('canActivate', isLoggendIn);
           return isLoggedIn;
       }
     }
