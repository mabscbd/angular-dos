import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinoApiclient } from './../models/destino-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidofavoritoAction } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(private destinosApiclient: DestinosApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store. (state => state.destinos.favorito)
     .subscribe(d => {
        if (d != null ) {
          this.updates.push('se ha elegido a' + d.nonbre);
        }
     });
     store.select(state => state.destinos.Items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }

  agregado(d: DestinoViaje) {
     this,this.destinosApiclient.add(d);
     this.onItemAdded.emit(d);
   }

   elegido(d: DestinoViaje) {
     this.destinosApiclient.elegir(e);
    }
  
  getALL() {}

  






