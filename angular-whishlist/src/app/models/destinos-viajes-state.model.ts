import {Injectable } from '@angular/core';
import {Action, State } from '@ngrx/store';
import {Actions, Effect, ofType } from '@ngrx/effects';
import {Observable, of } from 'rxjs';
import {map } from 'rxjs/operators';
import {DestinoViaje } from './destino-viaje.model';

//ESTADO
export interface DestinosViajesState {
    Items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export const intializeDestinosViajesState = function(){
    return {
        Items:[],
        loading: false,
        favorito: null
    };
};

//ACCIONES
export enum DestinoViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWM = '[Destinos Viajes] Vote Dowm'
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesTActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class ElegidofavoritoAction implements Action {
    type = DestinoViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino_: DestinoViaje) {}
}

export class VoteUpAction implements Action {
    type = DestinoViajesActionTypes.VOTE_UP;
    constructor(public destino_: DestinoViaje) {}
}

export class VoteDown implements Action {
    type = DestinoViajesActionTypes.VOTE_DOWM;
    constructor(public destino_: DestinoViaje) {}
}

export type DestinosViajesAction = NuevoDestinoAction | ElegidofavoritoAction
| VoteUpAction | VoteDownAction;

// REDUCERS
export function redecerDestinosViajes{
    state: DestinosViajesState,
    action: DestinoViajesActions 
):  DestinosViajesState {
    switch (Action.type) {
        case DestinoViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...State,
                items: [...State.items, (action as NuevoDestinoAction).destino ]
            };
        }
        case DestinoViajesActionTypes.ELEGIDO_FAVORITO; {
            state.items.forEach(x => x.setSelected(false));
            let fav:DestinoViaje = (action as ElegidofavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...sState,
                favorito: fav
            };
        }
        case DestinoViajesActionTypes.VOTE_UP; {
            const d: DestinoViaje = (action as VoteUpAction).destino;
            d.voteUp();
            return { ...sState,};
    }
    case DestinoViajesActionTypes.VOTE_DOWN; {
        const d: DestinoViaje = (action as VoteDownAction).destino;
        d.VoteDown();
        return { ...sState,};
}

   }
   return State;
}

//EFFECTS
@Injectable()
export class DestinoViajesEffects (
    @Effect()
nuevoAgregado$: ObservableAction> = this.actions$.pipe(
    ofType(DestinoViajesActionTypes.NUEVO_DESTINO),
    map((action:NuevoDestinoAction) => new ElegidofavoritoAction(action.destino))
);

constructor(private actions$: Action) {}
}
